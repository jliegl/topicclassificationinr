
plotTopicDistribution <- function(corp) {
  topics_covered = c()
  for (i in 1:length(corp) ) {
    topics_covered = c(topics_covered,meta(corp[[i]], tag="topics"))
  }
  barplot(table(topics_covered))
}

# neg-corpus before pruning
plotTopicDistribution(topicCorpusTrainPos)

# neg-corpus before pruning
plotTopicDistribution(topicCorpusTrainNeg)

# neg-corpus after pruning
plotTopicDistribution(topicCorpusTrainNegDocLimitPerTopic)

# final training corpus
plotTopicDistribution(svmPosNegTrainCorpus)

# pos-test corpus
plotTopicDistribution(topicCorpusTestPos)

# neg-test corpus
plotTopicDistribution(topicCorpusTestNeg)

# neg-test corpus with well-distributed topics
plotTopicDistribution(topicCorpusTestNegDocLimitPerTopic)

