# Author:  Johannes Liegl
# Date:    22.11.2014
# License: LGPL

# tested with these package-versions: XML_3.98-1.1, SparseM_1.05, slam_0.1-32, e1071_1.6-4, class_7.3-11, tm_0.6, NLP_0.1-5, hash_2.2.6

install.packages("hash")
install.packages("tm")
install.packages("class")
install.packages("e1071")
install.packages("slam")
install.packages("SparseM")
install.packages("XML")

library("NLP")
library("tm")
library("class")
library("e1071")
library("slam")
library("SparseM")
library("XML")
library("hash")

source("patchedReader.r")

# some functions 
topicFilter <- function(doc, topic, topicOfDoc) {  
  topics = meta(doc, tag="topics")
  if (length(topics) > 0) {
    topic %in% topics == topicOfDoc
  } else {
    FALSE
  }
}

mColRow <- function (m, mRow, mCol) {
  retVal <- m[(dim(m)[1]*(mCol-1)) + mRow]    
}

mColRowSparse <- function (m, mRow, mCol) {
  retVal <- m[(dim(m)[1]*(mCol-1)) + mRow]    
}

weightTfc <- function(m) {
  isDTM <- inherits(m, "DocumentTermMatrix")
  
  if (isDTM) 
    m <- t(m)
  
  sm <- m
  class(sm) <- "simple_triplet_matrix"
  idfVec <- log2(nDocs(m)/row_sums(m > 0))  
  curCol <- 1  
  rows <- dim(m)[1]
  cols <- dim(m)[2]  
  normVec <- rep(1, cols)
  
  while (curCol <= cols) {
    
    curRow <- 1
    curNormSum <- 0
    
    while (curRow <= rows) {
      tempVal <- mColRow(sm, curRow,curCol)
      
      if (!is.na(tempVal)) {      
        tempVal <- tempVal * idfVec[curRow]        
        tempVal <- tempVal * tempVal                
      } else {
        tempVal <- 0
      }
      
      curNormSum <- curNormSum + tempVal
      
      curRow <- curRow + 1
    }
    
    curNormSum <- curNormSum^0.5
    
    if ( curNormSum == 0)
      curNormSum = 0.001
    
    normVec[curCol] <- 1 / curNormSum
    
    curCol <- curCol + 1
  }
  
  m <- m * idfVec
  m <- t(m)
  m <- m * normVec
  m <- t(m)
  
  if (isDTM) 
    t(m)
  else 
    m
}

weightTfc <- WeightFunction(weightTfc, "tfc weighting", "tfc")


weightTfcSparse <- function(m) {
  isDTM <- inherits(m, "DocumentTermMatrix")
  
  if (isDTM) 
    m <- t(m)
  
  sm <- m
  class(sm) <- "simple_triplet_matrix"
  
  idfVec <- log2(nDocs(m)/row_sums(m > 0))  
  
  cols <- dim(m)[2]  
  curCol <- sm$j[1]
  curRow <- sm$i[1]
  curNormSum <- 0
  k <- 1 
  normVec <- rep(1, cols)
  
  while (k <= length(sm$j)) {              
    tempVal <- sm$v[k]    
    tempVal <- tempVal * idfVec[curRow]        
    tempVal <- tempVal * tempVal                          
    curNormSum <- curNormSum + tempVal            
    
    k <- k + 1    
    if (k > length(sm$j) || curCol != sm$j[k]) {
      curNormSum <- curNormSum^0.5
      
      # just in case, should actually never be 0
      if (curNormSum == 0)
        curNormSum = 0.001
      
      normVec[curCol] <- 1 / curNormSum
      curNormSum <- 0        
    }
    
    curCol <- sm$j[k]
    curRow <- sm$i[k]
  }
  
  m <- m * idfVec
  m <- t(m)
  m <- m * normVec
  m <- t(m)
  
  if (isDTM) 
    t(m)
  else 
    m
}

weightTfcSparse <- WeightFunction(weightTfcSparse, "tfc weighting sparse", "tfc")                                       


transformDocumentTermMatrixToCompressedSparseRowMatrix <- function (m) {
  isTDM <- inherits(m, "TermDocumentMatrix")
  
  if (isTDM) 
    m <- t(m)
  
  mV <- as.numeric(m$v)
  
  k <- 1
  tmpRa <- mV  
  tmpJa <- m$j
  tmpIa <- c()  
  
  colStart <- 0
  
  while (k < length(m$i)) {    
    tempVal <- m$i[k]    
    
    if (tempVal > colStart) {
      
      while (colStart < tempVal) {
        tmpIa <- c(tmpIa, k)        
        colStart <- colStart + 1
      }  
      
    }
    
    k <- k + 1
  }    
  
  tmpIa <- c(tmpIa, length(m$i) + 1)
  
  csr <- new("matrix.csr", ra = tmpRa, ja = as.integer(tmpJa), ia = as.integer(tmpIa), dimension = as.integer(c(dim(m)[1],dim(m)[2])))  
  
  if (isTDM)     
    t(csr)
  else 
    csr
}

maxDocNumPerTopicFilter <- function(doc, maxDocsPerTopic, hash) {
  res = TRUE
  topics = meta(doc, tag="topics")
  i = 1
      
  while (i <= length(topics)) {
    curTopic = topics[i]

    if (!has.key(curTopic, hash)) {

      hash[[curTopic]] = 1
      
    } else if ((hash[[curTopic]] + 1) > maxDocsPerTopic) {
      
      res = FALSE
      break
      
    } else {
      
      hash[[curTopic]] = hash[[curTopic]] + 1
      
    }
    
    i = i + 1
  }
  
  res
}
                       

# commands to be executed

baseDir <- "./"

reuters21578TrainDir <- DirSource(paste(baseDir,"reuters-21578-xml-train",sep=""))
reuters21578TestDir <- DirSource(paste(baseDir,"reuters-21578-xml-test",sep=""))

reuters21578TrainCorpus <- Corpus(reuters21578TrainDir, readerControl = list(reader = patchedReadReut21578XMLasPlain))
reuters21578TestCorpus <- Corpus(reuters21578TestDir, readerControl = list(reader = patchedReadReut21578XMLasPlain))

trainCorpTM <- DocumentTermMatrix(reuters21578TrainCorpus, control = list(stopwords = TRUE, removePunctuation = TRUE, removeNumbers = TRUE, bounds = list(global = c(3,Inf)), weighting = weightTf))
dict <- Terms(trainCorpTM)

topic <- "earn"
maxDocs <- 300

# Train Corpus Split for a given topic-Class (Pos/Neg)                               
topicCorpusTrainPos <- tm_filter(reuters21578TrainCorpus, FUN=topicFilter, topic, TRUE)                                         
topicCorpusTrainNeg <- tm_filter(reuters21578TrainCorpus, FUN=topicFilter, topic, FALSE)   

topic_hash = hash()
topicCorpusTrainNegDocLimitPerTopic <- tm_filter(topicCorpusTrainNeg, FUN=maxDocNumPerTopicFilter, maxDocsPerTopic = 5, hash = topic_hash)

# merge the corpora - just take 300 of positive instances because of the SVM model size
svmPosNegTrainCorpus <- c(topicCorpusTrainPos[1:min(maxDocs,length(topicCorpusTrainPos))], topicCorpusTrainNegDocLimitPerTopic)     

# construct corpus with one element containing all terms in the training corpus - needed for SVM
docs <- c(paste(dict, collapse = " "))
vs <- VectorSource(docs)
singleDocCorpus <- Corpus(vs)

svmPosNegTrainCorpusI <- c(svmPosNegTrainCorpus, singleDocCorpus)

# construct the matrix
xx <- DocumentTermMatrix(svmPosNegTrainCorpusI, control = list(weighting = weightTfcSparse, dictionary = dict))

# strip of the artificial last document and learn a model
xxi <- xx[1:dim(xx)[1]-1,]
xxi <- transformDocumentTermMatrixToCompressedSparseRowMatrix(xxi)    

# create the label vector for svm learning
yPos <- rep(1, length(topicCorpusTrainPos[1:min(maxDocs,length(topicCorpusTrainPos))]))
yNeg <- rep(-1, length(topicCorpusTrainNegDocLimitPerTopic))
y <- c(yPos, yNeg)  

model <- svm(xxi, y, type="C-classification", kernel = "linear", gamma = 1, degree = 2)


# Evaluation

# Test Corpus Split for a given topic-class (Pos/Neg)
topicCorpusTestPos <- tm_filter(reuters21578TestCorpus, FUN=topicFilter, topic, topicOfDoc = TRUE)
topicCorpusTestNeg <- tm_filter(reuters21578TestCorpus, FUN=topicFilter, topic, topicOfDoc = FALSE)   
topic_hash = hash()
topicCorpusTestNegDocLimitPerTopic <- tm_filter(topicCorpusTestNeg, FUN=maxDocNumPerTopicFilter, maxDocsPerTopic = 5, hash = topic_hash)

svmPosNegTestCorpus <- c(topicCorpusTestPos[1:min(maxDocs,length(topicCorpusTestPos))], topicCorpusTestNegDocLimitPerTopic) 
svmPosNegTestCorpusI <- c(svmPosNegTestCorpus, singleDocCorpus)

# create the label vector for svm learning
yTestPos <- rep(1, length(topicCorpusTestPos[1:min(maxDocs,length(topicCorpusTestPos))]))
yTestNeg <- rep(-1, length(topicCorpusTestNegDocLimitPerTopic))
yTest <- c(yTestPos, yTestNeg)

xxT <- DocumentTermMatrix(svmPosNegTestCorpusI, control = list(weighting = weightTfcSparse, dictionary = dict))

# Transform the simple triplet matrix into a compressed sparse row matrix
xxTi <- xxT[1:dim(xxT)[1]-1,]
xxTi <- transformDocumentTermMatrixToCompressedSparseRowMatrix(xxTi)

# apply the model on the test data
svmPrediction <- predict(model, xxTi)

# re-labelling: 1 -> topic and -1 -> notTopic
notTopic <- paste('not', topic, sep = '')
svmPredictionI <- as.numeric(levels(svmPrediction))[svmPrediction]
svmPredictionI[which(svmPredictionI == -1)] = notTopic
svmPredictionI[which(svmPredictionI == 1)] = topic

yTestI <- yTest
yTestI[which(yTestI == -1)] = notTopic
yTestI[which(yTestI == 1)] = topic

svmTable <- table(svm = factor(svmPredictionI, levels = c(topic, notTopic)), reuters=factor(yTestI, levels = c(topic,notTopic)))

precision <-  svmTable[1,1] / ( svmTable[1,1] + svmTable[1,2] + 0.0001)
recall <- svmTable[1,1] / ( svmTable[1,1] + svmTable[2,1] + 0.0001)
accuracy <- sum(diag(svmTable)) / sum(svmTable)

print(paste('topic: ', 'earn', 'precision: ', precision, 'recall: ', recall, ' '))
