patchedReut21578XMLSpec <-
  list(author = list("node", "/REUTERS/TEXT/AUTHOR"),
       datetimestamp = list("function", function(node)
         strptime(sapply(XML::getNodeSet(node, "/REUTERS/DATE"),
                         XML::xmlValue),
                  format = "%d-%B-%Y %H:%M:%S",
                  tz = "GMT")),
       description = list("unevaluated", ""),
       heading = list("node", "/REUTERS/TEXT/TITLE"),
       id = list("attribute", "/REUTERS/@NEWID"),       
       lewissplit = list("attribute", "/REUTERS/@LEWISSPLIT"),
       cgisplit = list("attribute", "/REUTERS/@CGISPLIT"),
       oldid = list("attribute", "/REUTERS/@OLDID"),
       origin = list("unevaluated", "Reuters-21578 XML"),
       topics = list("node", "/REUTERS/TOPICS/D"),
       places = list("node", "/REUTERS/PLACES/D"),
       people = list("node", "/REUTERS/PEOPLE/D"),
       orgs = list("node", "/REUTERS/ORGS/D"),
       exchanges = list("node", "/REUTERS/EXCHANGES/D"))

patchedReadReut21578XMLasPlain <-
  readXML(spec = c(patchedReut21578XMLSpec,
                   list(content = list("node", "/REUTERS/TEXT/BODY"))),
          doc = PlainTextDocument())